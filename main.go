package main

import (
	"bytes"
	"fmt"
	"github.com/Pallinder/go-randomdata"
	"gopkg.in/yaml.v2"
	"io/fs"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func genISBN() string {
	sum := 0
	var d1, d2, d3, d4, d5, d6, d7, d8, d9, d0 int
	for {
		d1 = 0
		d2 = randomdata.Number(10)
		d3 = randomdata.Number(10)
		d4 = randomdata.Number(10)
		d5 = randomdata.Number(10)
		d6 = randomdata.Number(10)
		d7 = randomdata.Number(10)
		d8 = randomdata.Number(10)
		d9 = 9
		d0 = randomdata.Number(10)
		sum = (d1 * 10) + (d2 * 9) + (d3 * 8) + (d4 * 7) + (d5 * 6) + (d6 * 5) + (d7 * 4) + (d8 * 3) + (d9 * 2) + d0
		if (sum % 11) == 0 {
			//log.Println("isbn valid")
			break
		} else {
			//log.Println("isbn invalid")
			break
		}
	}
	s1 := strconv.Itoa(d1)
	s2 := strconv.Itoa(d2)
	s3 := strconv.Itoa(d3)
	s4 := strconv.Itoa(d4)
	s5 := strconv.Itoa(d5)
	s6 := strconv.Itoa(d6)
	s7 := strconv.Itoa(d7)
	s8 := strconv.Itoa(d8)
	s9 := strconv.Itoa(d9)
	s0 := strconv.Itoa(d0)
	s := s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8 + s9 + s0
	return s
}

func main() {
	//load json template file
	templateFile, err := fs.ReadFile(os.DirFS("."), "template.json")
	if err != nil {
		log.Fatal(err)
		log.Println(templateFile)
	}

	//load yaml config file
	configFile, err := fs.ReadFile(os.DirFS("."), "config.yml")
	if err != nil {
		log.Fatal(err)
		log.Println(configFile)
	}

	//map yaml config to interface
	var testinter interface{}
	err = yaml.Unmarshal(configFile, &testinter)
	if err != nil {
		log.Fatal(err)
	}

	strIter := os.Getenv("ITER")
	if strIter == "" {
		strIter = "0"
	}
	iter, err := strconv.Atoi(strIter)
	if err != nil {
		log.Fatal(err)
	}
	counter := 0
	for counter <= iter {
		jsonOut := string(templateFile)
		for k, v := range testinter.(map[interface{}]interface{}) {
			//k = var1
			//log.Println("k:", k)
			//v = map[data:[map[kind:number] map[range:map[rangeHigh:2177 rangeLow:1101]]]]
			//log.Println("v:", v)

			r := regexp.MustCompile(`[^0-9]`)
			varNum := r.ReplaceAllString(k.(string), "")
			//log.Println("varNum:", varNum)

			for _, j := range v.(map[interface{}]interface{}) {
				//i = data
				//log.Println("i:", i)
				//j = [map[kind:number] map[range:map[rangeHigh:2177 rangeLow:1101]]]
				//log.Println("j:", j)

				kind := ""
				for x, y := range j.([]interface{}) {
					//x = 0, 1
					//log.Println("x:", x)
					//y = map[kind:number]
					//log.Println("y:", y)
					if x%2 == 0 {
						//y = map[kind:number]
						for w, z := range y.(map[interface{}]interface{}) {
							//log.Println("w:", w)
							//log.Println("z:", z)
							if w.(string) == "kind" {
								kind = z.(string)
							}
						}
					} else {
						//y = map[range:map[rangeHigh:2177 rangeLow:1101]]
						for w, z := range y.(map[interface{}]interface{}) {
							//w = range
							//log.Println("w:", w)
							//z = z: map[rangeHigh:2177 rangeLow:1101]
							//log.Println("z:", z)
							if kind == "boolean" {
								randBool := strconv.FormatBool(randomdata.Boolean())
								jsonOut = strings.Replace(jsonOut, "%"+varNum+"%", randBool, -1)
							}
							if w.(string) == "type" && z.(string) == "isbn" {
								isbn := genISBN()
								jsonOut = strings.Replace(jsonOut, "%"+varNum+"%", isbn, -1)
							}
							if w.(string) == "type" && z.(string) == "silly" {
								sillyName := randomdata.SillyName()
								jsonOut = strings.Replace(jsonOut, "%"+varNum+"%", sillyName, -1)
							}
							if w.(string) == "type" && z.(string) == "first" {
								firstName := randomdata.FirstName(randomdata.Female)
								jsonOut = strings.Replace(jsonOut, "%"+varNum+"%", firstName, -1)
							}
							if w.(string) == "type" && z.(string) == "middle" {
								midName := randomdata.LastName()
								jsonOut = strings.Replace(jsonOut, "%"+varNum+"%", midName, -1)
							}
							if w.(string) == "type" && z.(string) == "last" {
								lastName := randomdata.LastName()
								jsonOut = strings.Replace(jsonOut, "%"+varNum+"%", lastName, -1)
							}
							if w.(string) == "range" {
								rL := 0
								rH := 0
								for rangeType, rangeValue := range z.(map[interface{}]interface{}) {
									//log.Println("rangeType:", rangeType)
									//log.Println("rangeValue:", rangeValue)
									if rangeType.(string) == "rangeLow" {
										rL = rangeValue.(int)
									}
									if rangeType.(string) == "rangeHigh" {
										rH = rangeValue.(int)
									}
								}
								//log.Println("rL:", rL)
								//log.Println("rH:", rH)
								generatedNum := randomdata.Number(rL, rH)
								jsonOut = strings.Replace(jsonOut, "%"+varNum+"%", strconv.Itoa(generatedNum), -1)
							}
						}
					}
				}
			}
		}
		fmt.Print(jsonOut)
		time.Sleep(10 * time.Millisecond)
		postURL := os.Getenv("POST_URL")
		if postURL != "" {
			resp, err := http.Post(postURL, "application/json", bytes.NewBuffer([]byte(jsonOut)))
			if err != nil {
				log.Fatal("POST error:", err)
			}
			resp.Body.Close()
		}
		counter = counter + 1
	}
}
